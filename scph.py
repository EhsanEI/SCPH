'''
Created on Aug 21, 2016

@author: ehsan
'''
from fuel.datasets.hdf5 import H5PYDataset
import os
import numpy as np
import scipy as sp
from sklearn.cluster import MiniBatchKMeans, KMeans
from scipy.sparse.csr import csr_matrix
from scipy.sparse.construct import spdiags, eye
from evaluation import computeKnn, computeMap
from scipy.sparse.lil import lil_matrix
from numpy.linalg.linalg import svd
from copy import deepcopy, copy
import warnings
from sklearn.preprocessing import normalize
import gist
from skimage.feature import hog
from skimage import data, color, exposure

class AttrDict(dict):
	def __init__(self, *args, **kwargs):
		super(AttrDict, self).__init__(*args, **kwargs)
		self.__dict__ = self
		
def transform(X, km, W, R, p):
	dists = km.transform(X)
	threshold = np.sort(dists,axis=1)[:,p.s] - 1e-7
	mask = dists<threshold[:,np.newaxis]
	
	Z = np.exp(-(dists**2)/p.t)
	Z = Z*mask
	Z = Z/Z.sum(axis=1)[:, np.newaxis]
	Z = Z-Z.mean(axis=0)
	
	V = Z.dot(W)
	Y = np.sign(V.dot(R))
	return Y
	
	
def SCPH(trainX, first_ind, pair_ind, is_sim, targets, p):
	# Sparse Embedding
	print 'Sparse Embedding'
	km = MiniBatchKMeans(batch_size=100,n_clusters=p.m)
	with warnings.catch_warnings():
		warnings.simplefilter("ignore")
		dists = km.fit_transform(trainX)

	threshold = np.sort(dists,axis=1)[:,p.s] - 1e-7
	mask = dists<threshold[:,np.newaxis]
	
	Z = np.exp(-(dists**2)/p.t)
	Z = Z*mask
	Z = Z/Z.sum(axis=1)[:, np.newaxis]
	Z = Z-Z.mean(axis=0)
	
	# Constraints Preserving Embedding
	print 'Constraints Preserving Embedding'
	S_tilde = lil_matrix((trainX.shape[0],trainX.shape[0]))
	
	print '-- Adding constraints'
	for i in xrange(p.labeled_cnt):
		
		if is_sim[i] == 1:
			score = -1
		else:
			score = 1
		
		S_tilde[first_ind[i],pair_ind[i]] = score
		S_tilde[pair_ind[i],first_ind[i]] = score
		

	# I'm assuming that number of similar and dissimilar pairs are equal
	S_tilde /= (S_tilde==-1).sum()/2
	
	D = np.asarray(S_tilde.sum(axis=1)).squeeze()
	D = spdiags(D, 0, D.shape[0], D.shape[0])
	L = D-S_tilde
	LL = L + (p.lam/p.train_cnt)*eye(L.shape[0])

	M = Z.T * LL
	M = M.dot(Z)
	
	print '-- Computing W'
	_, W = sp.linalg.eigh(M, eigvals=(M.shape[0]-p.r,M.shape[0]-1))
	
	# Quantization
	print 'Quantization'
	V = Z.dot(W)
	R = np.eye(V.shape[1])
	prev_res_Y = None
	for iter_num in xrange(p.max_iter):
		tmp = (eye(S_tilde.shape[0])-p.eta*S_tilde).dot(V)
		Y = np.sign(tmp.dot(R))
		u,_,vt = svd(Y.T.dot(tmp))
		R = vt.T.dot(u.T)
		
		res_Y = np.sign(V.dot(R))
		print '-- Iteration:', iter_num+1
		mapp = computeMap(res_Y[:1000,:], res_Y[:1000,:], targets[:1000,:], targets[:1000,:])
		print 'MAP:', mapp
		print 'Changes in Y in this iteration:', np.sum(res_Y != prev_res_Y)
		if (prev_res_Y != None) and (res_Y == prev_res_Y).all():
			print 'converged!'
			break 
		prev_res_Y = res_Y
	return km, W, R, mapp

if __name__ == '__main__':
	
	np.random.seed(42)
	
	p = AttrDict()
	p.m = 300
	p.s = 10
	p.t = 0.05
	p.lam = [0.0]
	p.r = 20
	p.eta = 1.0
	p.max_iter = 100
	p.normalize = False
	p.extract_gist = False
	p.extract_hog = True
	
	p.ds_name = 'svhn'
	p.rep = 0
	p.train_cnt = 40000
	p.valid_cnt = 6590
	p.test_cnt = 15950
	
	p.labeled_cnt = 100000
	
	p.train_ind = range(p.train_cnt)
	p.valid_ind = range(p.train_cnt, p.train_cnt+p.valid_cnt)
	p.test_ind = range(p.train_cnt+p.valid_cnt, p.train_cnt+p.valid_cnt+p.test_cnt)
	p.labeled_ind = range(p.labeled_cnt)
	
	constraints = H5PYDataset(os.path.join('dat',p.ds_name,p.ds_name+'_metric_100000_'+str(p.rep)+'.hdf5'), which_set=('all'), sources=['first_ind', 'pair_ind', 'is_sim'], load_in_memory=True)

	ds_train = H5PYDataset(os.path.join('dat',p.ds_name+'.hdf5'), which_set=('train'), sources=['features', 'targets'], load_in_memory=True)
	
	ds_test = H5PYDataset(os.path.join('dat',p.ds_name+'.hdf5'), which_set=('test'), sources=['features', 'targets'], load_in_memory=True)
					 
	ds_train_X = ds_train.data_sources[ds_train.sources.index('features')]
	ds_test_X = ds_test.data_sources[ds_test.sources.index('features')]
	ds_train_y = ds_train.data_sources[ds_train.sources.index('targets')]
	ds_test_y = ds_test.data_sources[ds_test.sources.index('targets')]
	if ds_train_y.ndim < 2:
		ds_train_y = ds_train_y[:,np.newaxis]
	if ds_test_y.ndim < 2:
		ds_test_y = ds_test_y[:,np.newaxis]
	X = np.vstack((ds_train_X, ds_test_X))
	y = np.vstack((ds_train_y, ds_test_y))
	
	if p.extract_gist:
		try:
			feats = np.load(os.path.join('dat', p.ds_name+'_feats.npy'))
			X = feats
		except:
			print 'started feature extraction'
			feats = np.zeros((X.shape[0],960))
			for i in xrange(X.shape[0]):
				if np.mod(i,1000)==0:
					print i
				feats[i,:] = gist.extract(X[i,...].astype(np.uint8).swapaxes(0,2))
			np.save(os.path.join('dat', p.ds_name+'_feats.npy'), feats)
			X = feats
			print 'finished feature extraction'
	elif p.extract_hog:
		try:
			feats = np.load(os.path.join('dat', p.ds_name+'_hog.npy'))
			X = feats
		except:
			print 'started feature extraction'
			feats = np.zeros((X.shape[0],32))
			for i in xrange(X.shape[0]):
				if np.mod(i,1000)==0:
					print i
				im = color.rgb2gray(X[i,...].astype(np.uint8).swapaxes(0,2))
				feats[i,:], _ = hog(im, orientations=8, pixels_per_cell=(16, 16), cells_per_block=(1, 1), visualise=True)
			np.save(os.path.join('dat', p.ds_name+'_hog.npy'), feats)
			X = feats
			print 'finished feature extraction'
	else:
		X = X.reshape((X.shape[0],-1))
	if p.normalize:
		X = normalize(X)
	
	trainX = X[p.train_ind,...]
	trainY = y[p.train_ind,...]
	
	first_ind = constraints.data_sources[constraints.sources.index('first_ind')][p.labeled_ind,...]
	pair_ind = constraints.data_sources[constraints.sources.index('pair_ind')][p.labeled_ind,...]
	is_sim = constraints.data_sources[constraints.sources.index('is_sim')][p.labeled_ind,...]
	
	validX = X[p.valid_ind,...]
	validY = y[p.valid_ind,...]
		
	print trainX.shape, trainY.shape, first_ind.shape, pair_ind.shape, validX.shape, validY.shape
	print computeMap(trainX[:1000,:], trainX[:1000,:], trainY[:1000,:], trainY[:1000,:])
	#~ print computeMap(validX, validX, validY, validY)
	
	results = []
	for lam in p.lam:
		current_p = copy(p)
		current_p.lam = lam
		print 'lambda:', current_p.lam
		km, W, R, _ = SCPH(trainX, first_ind, pair_ind, is_sim, trainY, current_p)
		print W.shape, R.shape
		
		valid_transform = transform(validX, km, W, R, p)
		result = computeMap(valid_transform, valid_transform, validY, validY)
		print result
		results = results + [result]
		
	print results
